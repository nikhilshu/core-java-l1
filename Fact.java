/*
1.	Write a program to print factorial of N ( without using any loop)
*/
import java.util.*;
public class Fact

{
public int fact(int num)
{
if(num==1)
{return 1;}
else
{
return num*fact(num-1);}
}
public static void main(String args[])
{
Fact obj=new Fact();
System.out.println("Enter the no.");
Scanner sc=new Scanner(System.in);
int n=sc.nextInt();
System.out.println("Factorial of "+n+" is : "+obj.fact(n));

}

}