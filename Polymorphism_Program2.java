/*
2.	There is an animal class which has the common characteristics of all animals. Dog, Horse, Cat are animals(sub-class). Each can shout, but each shout is different. Use polymorphism to create objects of same and using an animal variable, make each of the animals shout.
*/
import java.util.*;
abstract class Animal
{
abstract void shout();

}
class Dog extends Animal
{
void shout()
{
System.out.println("Dog is Barking");
}
}
class Cat extends Animal
{
void shout()
{
System.out.println("Mew");
}
}
class Horse extends Animal
{
void shout()
{
System.out.println("Neigh");
}
}
public class Polymorphism_Program2
{
public static void main(String args[])
{
Animal obj1=new Dog();
Animal obj2=new Cat();
Animal obj3=new Horse();
obj1.shout();
obj2.shout();
obj3.shout();



}

}

