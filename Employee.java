/*
7.	Create an ArrayList of Employee(id,name,address,sal) objects and search for particular Employee object based on id number and name.


*/


import java.util.*;
public class Employee{
int id;
String Name;
String Address;
int sal;
  public Employee(int id, String Name, String Address,int sal) {
     this.id=id;
this.Name=Name;
this.Address=Address;
this.sal=sal;
  }

public static void main(String args[]){
ArrayList<Employee> arr = new ArrayList<Employee>();
arr.add(new Employee(01, "Nikhil","Naini",20000));
arr.add(new Employee(02, "Sourav","Kolkata",13200));
arr.add(new Employee(03, "Rahul","Delhi",123000));
for ( Employee currEmp : arr)  
                        {  
                            if(currEmp.id==01 && currEmp.Name=="Nikhil")
                            {
                                System.out.println("Employee name is "+currEmp.Name);
                            }
                        }
}
}