/*
3.	Create an abstract class Instrument which is having the abstract function play. Create three more sub classes from Instrument which is Piano, Flute, Guitar.Override the play method inside all three classes printing a message 
�Piano is playing  tantantantan  �  for Piano class
�Flute is playing  toottoottoottoot�  for Flute class
�Guitar is playing  tintintin �  for Guitar class 
Create an array of 10 Instruments.
Assign different type of instrument to Instrument reference.
Check for the polymorphic behavior of  play method.

*/
import java.util.*;
abstract class Instrument
{
abstract void play();

}
class Piano extends Instrument
{
void play()
{
System.out.println("Piano is playing  tan tan tan tan");
}
}
class Flute extends Instrument
{
void play()
{
System.out.println("Flute is playing  toot toot toot toot");
}
}
class Guitar extends Instrument
{
void play()
{
System.out.println("Guitar is playing  tin  tin  tin");
}
}
public class PlayInstrument
{
public static void main(String args[])
{
Instrument obj[]=new Instrument[10];
obj[0]=new Flute();
obj[1]=new Piano();
obj[2]=new Guitar();
obj[3]=new Flute();
obj[4]=new Guitar();
obj[5]=new Flute();
obj[6]=new Piano();
obj[7]=new Flute();
obj[8]=new Guitar();
obj[9]=new Flute();
obj[0].play();
obj[1].play();
obj[2].play();
obj[3].play();
obj[4].play();
obj[5].play();
obj[6].play();
obj[7].play();
obj[8].play();
obj[9].play();



}

}

