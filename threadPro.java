/*
6.	Write  a  program  to  assign  the  current  thread  to  t1.  Change  the  name  of  the  thread  to  MyThread.  Display  the  changed  name  of  the  thread.  Also  it  should  display  the  current  time.  Put  the  thread  to  sleep  for  10  seconds  and  display  the  time  again.
*/


import java.util.*;
public class threadPro implements Runnable
{
public void run(){  
System.out.println("thread is running...");  
}  
public static void main(String args[])
{
Thread currThread = Thread.currentThread();
currThread.setName("MyThread");
System.out.println("Thread 1: " + currThread.getName());


try
{

for(int i=0;i<10;i++)
{
Date d=new Date();
System.out.println(d.toString());
currThread.sleep(10000);
}
}
catch(InterruptedException e)
{
System.out.println(e);
}
}
}